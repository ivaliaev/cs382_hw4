/*
 * transpif.c
 *
 *  Created on: Nov 8, 2018
 *      Author: Ivan
 *      "I pledge my Honor that I have abided by the Stevens Honor System"
 */

//Imports
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include <sys/time.h>

//Macros
#define DEBUG 1
#define TOGGLE 0
#ifdef DEBUG
	#define dbg_print(M, ...) fprintf( stdout, "DEBUG %s: %d: " M "\n", __FILE__, __LINE__, ##__VA_ARGS__)
#else
    #define dbg_print(M, ...)
#endif

//Utility: Gets the min of two numbers.
//Originally I was using fmin from math.h but that needs to be linked separately so I switched a macro
//for convenience
#define min(a,b) \
   ({ __typeof__ (a) _a = (a); \
       __typeof__ (b) _b = (b); \
     _a < _b ? _a : _b; })

//Utility: Generate a random float between min and max
float rand_range(float min, float max){
	float ret_val = min+((float)rand())/(((float)RAND_MAX)/(max-min));
	return ret_val;
}

//Utility: Fill a 1D Matrix with random floats
void rand_fill(float* start_loc,int quantity){
	for(int i=0;i<quantity;i++){
		start_loc[i]=rand_range(0,100);
		//start_loc[i]=(float)rand();
	}
}

//Utility: Print a Matrix
void print_matrix(float* start_loc,int width, int height){
	int quantity=width*height;
	for(int i=0;i<quantity;i++){
		fprintf(stdout,"%f\t",start_loc[i]);
		if((i+1)%width==0 && (i!=0 || width==1)){
			fprintf(stdout,"\n");
		}
	}
}

//Function: Time and transpose a matrix
void timed_transpose(float *in_loc, float *out_loc,int height, int width, int block_width){
	//Setup
	//Start Timer
	struct timeval start,stop;
	gettimeofday(&start,0);
	//Transpose
	for (int a = 0; a < height; a += block_width) {
		//NOTE: calculating and storing bound1, bound2 eliminates the need to use
		//		if statements to verify that the element to be accessed does not fall
		//		outside the scope of the current row
		int bound1 = min(a+block_width,height);
		for (int b = 0; b < width; b += block_width) {
			int bound2 = min(b+block_width,width);
			for (int c = a; c < bound1; ++c) {
				int prod1=c*width;
				for (int d = b; d < bound2; ++d) {
					//dbg_print("Out index: %d\tIn index:%d\n",c + d*width,d + prod1);
					out_loc[c + d*height] = in_loc[d + prod1];
				}
			}
		}
	}
	//End Transpose
	//End Timer
	gettimeofday(&stop,0);
	//Print Info
	fprintf(stdout,"Time Taken for %dx%d Tranpose with Block Size %d\n",height,width,block_width);
	fprintf(stdout,"Seconds: %ld\n",(stop.tv_sec-start.tv_sec));
	fprintf(stdout,"Microseconds: %ld\n",(stop.tv_usec-start.tv_usec));
	fprintf(stdout,"Total Time in Microseconds: %ld\n",(stop.tv_sec-start.tv_sec)*1000000+(stop.tv_usec-start.tv_usec));

}

//Main: Sets up the shell within a shell
int main(int argc, char **argv){
	//----------------------------------------------------------------------
	//Setup:
	//Check for correct number of parameters
	if (argc != 4){
		fprintf(stderr, "Wrong number of command-line arguments\n");
		fprintf(stderr, "Usage: transpif <HEIGHT> <WIDTH> <BLOCK WIDTH>\n");
		return 1;
	}
	//Parse Params
	int height = atoi(argv[1]);
	int width = atoi(argv[2]);
	int block_width=atoi(argv[3]);
	if(height<=0 ||width<=0 ||block_width<=0 ){
		fprintf(stderr, "All parameters must be natural numbers, aborting\n");
		return 1;
	}
	//Allocate Space
	int w2=height*width;
	float *input = malloc(w2*sizeof(float));
	float *output = malloc(w2*sizeof(float));
	//Seed Rand
	srand(time(NULL));
	//Fill input matrix
	rand_fill(input,w2);
	//----------------------------------------------------------------------
	//Functionality:
	if(TOGGLE){
		fprintf(stdout,"Input Matrix:\n");
		print_matrix(input,width,height);
	}
	//fprintf(stdout,"%d",min(5,5));
	timed_transpose(input,output,height, width,block_width);
	if(TOGGLE){
		fprintf(stdout,"Output Matrix:\n");
		print_matrix(output,height,width);
	}
	//----------------------------------------------------------------------
	//Cleanup:
	if(input){
		free(input);
	}
	if(output){
		free(output);
	}
	return 0;
}
