/*
 * transp.c
 *
 *  Created on: Nov 7, 2018
 *      Author: Ivan
 *      "I pledge my Honor that I have abided by the Stevens Honor System"
 */

//Imports
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include <sys/time.h>

//Macros
#define DEBUG 1
#define TOGGLE 0
#ifdef DEBUG
	#define dbg_print(M, ...) fprintf( stdout, "DEBUG %s: %d: " M "\n", __FILE__, __LINE__, ##__VA_ARGS__)
#else
    #define dbg_print(M, ...)
#endif

//Utility: Generate a random float between min and max
float rand_range(float min, float max){
	float ret_val = min+((float)rand())/(((float)RAND_MAX)/(max-min));
	return ret_val;
}

//Utility: Fill a 1D Matrix with random floats
void rand_fill(float* start_loc,int quantity){
	for(int i=0;i<quantity;i++){
		start_loc[i]=rand_range(0,100);
		//start_loc[i]=(float)rand();
	}
}

//Utility: Print a Matrix
void print_matrix(float* start_loc,int width, int height){
	int quantity=width*height;
	for(int i=0;i<quantity;i++){
		fprintf(stdout,"%f\t",start_loc[i]);
		if((i+1)%width==0 && (i!=0 || width==1)){
			fprintf(stdout,"\n");
		}
	}
}

//Function: Time and transpose a matrix
void timed_transpose(float *in_loc, float *out_loc,int width, int block_size){
	//Setup
	//Start Timer
	struct timeval start,stop;
	gettimeofday(&start,0);
	//Transpose
	for (int a = 0; a < width; a += block_size) {
	    for (int b = 0; b < width; b += block_size) {
	        for (int c = a; c < a + block_size; ++c) {
	        	int prod1=c*width;
	            for (int d = b; d < b + block_size; ++d) {
	            	//dbg_print("a: %d\tb: %d\tc: %d\td: %d",a,b,c,d);
	            	//dbg_print("input value: %f",in_loc[d+prod1]);
	            	//dbg_print("Out index: %d\tIn index:%d\n",c + d*width,d + prod1);
	                out_loc[c + d*width] = in_loc[d + prod1];
	            }
	        }
	    }
	}
	//End Transpose
	//End Timer
	gettimeofday(&stop,0);
	//Print Info
	fprintf(stdout,"Time Taken for %dx%d Tranpose with Block Size %d\n",width,width,block_size);
	fprintf(stdout,"Seconds: %ld\n",(stop.tv_sec-start.tv_sec));
	fprintf(stdout,"Microseconds: %ld\n",(stop.tv_usec-start.tv_usec));
	fprintf(stdout,"Total Time in Microseconds: %ld\n",(stop.tv_sec-start.tv_sec)*1000000+(stop.tv_usec-start.tv_usec));

}

//Main: Sets up the shell within a shell
int main(int argc, char **argv){
	//----------------------------------------------------------------------
	//Setup:
	//Check for correct number of parameters
	if (argc != 3){
		fprintf(stderr, "Wrong number of command-line arguments\n");
		fprintf(stderr, "Usage: transp <WIDTH> <BLOCK SIZE>\n");
		return 1;
	}
	//Parse Params
	int width = atoi(argv[1]);
	int block_size=atoi(argv[2]);
	if(width<=0 ||block_size<=0 ){
		fprintf(stderr, "All parameters must be natural numbers, aborting\n");
		return 1;
	}
	//Allocate Space
	int w2=width*width;
	float *input = malloc(w2*sizeof(float));
	float *output = malloc(w2*sizeof(float));
	//Seed Rand
	srand(time(NULL));
	//Fill input matrix
	rand_fill(input,w2);
	//----------------------------------------------------------------------
	//Functionality:
	if(TOGGLE){
		fprintf(stdout,"Input Matrix:\n");
		print_matrix(input,width,width);
	}
	timed_transpose(input,output,width,block_size);
	if(TOGGLE){
		fprintf(stdout,"Output Matrix:\n");
		print_matrix(output,width,width);
	}
	//----------------------------------------------------------------------
	//Cleanup:
	if(input){
		free(input);
	}
	if(output){
		free(output);
	}
	return 0;
}
